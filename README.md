# Overview

This document provides an overview of the repository setup and way-of-working. A basic understanding of NetSuite's SuiteCloud offering and version control using git is assumed. Reach out to your colleagues (e.g. Chidi Okwudire or Marko Obradovic) for further assistance.

## One-time repository configuration
When initializing a new client repository using this template (typically done by Marty), the following steps are recommended and can all be executed directly from Gitlab:

 - Create an client repository by cloning the template repository as explained in the [Gitlab Import Repository By URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) guide. The resulting repository will have the defacto SuiteClould project structure (i.e. similar to running `suitecloud project:create -i` from the CLI tool). This provides a stable and consistent setup for all projects.
 - Make the following updates right from Gitlab. (Navigate to the client project in Gitlab) >> Repository >> Files >> (Select a file to edit) >> Edit in Web IDE >> Commit:
   - Update the `<projectname>change-me-to-client-name</projectname>`  in `src/manifest.xml` to the client name.
   - [Optional] Update the "url" field in `package.json` to the URL of the client's repo.
   - Create branch `working` from `main`.
   - In the project in Gitlab, go to `Settings >> Repository >> Protected branches` and make sure that `main` is protected *and* is configured as:
       - Allowed to merge: Developers + Maintainers (basically all consultants).
       - Allowed to push: No one (this forces us to always merge changes as recommended in the way of working below).

## Repository usage
Once the repository has been configured as described above, any consultant working for the client can follow the steps outlined in this section to use that repository to manage their artifacts as described in this section.

### Project setup / import
This repository include the necessary configuration to use the [SuiteCloud CLI for Node.js](https://system.app.netsuite.com/app/help/helpcenter.nl?fid=chapter_1558708800.html). If you have not yet set up SuiteCloud CLI on your development machine, visit the preceding link for one-time setup instructions.

The repository also includes [ESLint](https://eslint.org/) configuration which is highly recommended to help catch basic JavaScript error. Again, this will require one-time setup. Sufficient guidance is available online.

> As of December 2021, we removed support for Eclipse as the defacto IDE supported by NetSuite is now Visual Studio (VS) Code. See instructions below for VS Code setup and extensions.

If VS Code is not your preferred IDE you can use still use the SuiteCloud CLI on the command line.
> Once SuiteCloud CLI is set up on your machine, all you need to do is navigate to the root of your checked out local repository e.g. `cd G:\My Drive\dev\prolecto\clients\client-name` and run `suitecloud account:setup -i` to generate your authentication token. After that, you're able to perform SDF operations on that account e.g. list files, get files, push files, deploy the SDF project, etc. Document with CLI commands for nodejs is found [here](https://docs.google.com/document/d/1ZRBAWAywIiLG8W19xbJNfzZpdql146A63utODhrdAdw/edit#heading=h.3nr1dyjyzuno).

### ESLint setup
ESLint is handy tool for identifying and reporting issues in your code. A basic configuration file is provided in this repository. Search online for how to enable ESLint in Eclipse or whatever editor you use for development (e.g. VS Code).

### Recommended VS Code Extensions
- [SuiteCloud Extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=Oracle.suitecloud-vscode-extension): NetSuite official extension for developing and deploying SuiteCloud Projects with SuiteCloud Development Framework (SDF).
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint): Integrates ESLint JavaScript into VS Code.
- [SuiteSnippets](https://marketplace.visualstudio.com/items?itemName=ericbirdsall.SuiteSnippets): Community extension for SuiteScript 2.x code completion.

## Git workflow guidelines
The recommended branching structure is captured in [this draw.io document](https://app.diagrams.net/#G1by21MG351XSj8_NLyB1L0xkAtAvF4j5x) prepared and managed by Marko Obradovic. We understand that it takes time to mature into these practices. As such, we will work gradually towards this end goal. Reach out if you have questions or concerns.

> This workflow is simply a guide and may need to be adapted based on specific project needs.

> For older projects, branch `main` may be named `master`.

Here's an overview of the basic process:
- Create a separate task branch from `main` for every PTM task. The branch name should start with the PTM task Id, typically followed by a short descriptive string, e.g., PTM621014-my-awesome-feature. 
  - As a best practice, always prepend all your commits with your PTM task Id. This will make it easier to identify commits related to this feature when your branch is subsequently (squashed and) merged later on.
- Work from your branch until your work is ready for QA at which point it will be merged into the `working` branch.
- Once QA approved, and after code is deployed to production, merge your task branch (*not* `working`) into `main` and tag it (e.g. vYYYYMMDD) to represent a released deliverable. With this flow, releases to Production will typically only occur from the `main` branch.
- Delete your task branch once it's merged. If you need to revisit that task, you simply create a new branch with the same name. This approach reduces clutter as well as the need to maintain stale/completed task branches.
- Refresh all open task branches from `main` to reflect the latest state and minimize merge conflicts later.

We recommend regularly pulling in the XML definitions of account objects that your code depends on for safekeeping in git. This is crucial to a successful collaboration especially when the same Sandbox environment is used by multiple developers and/or as both a development and QA environment. The SuiteCloud CLI makes this process easy as you can bulk import multiple objects.

> In some cases, it might be useful to have a persistent branch (e.g. `sb1-backup` or `prod-backup`) that is a full environment sync of all scripts and object in a given environment. Such a branch will never be merged but will provide a reference for recovery / facilitate tracking general changes in the environment. The update frequency of such a backup branch willl typically once per Production releases or right before major changes.

## Command line instructions

You can also upload existing files from your computer using the instructions below. There is an abundance of Git resources online for other tasks.

> If you are more visually inclined and prefer to avoid the command line, there are several free Git GUIs out there e.g. [Sourcetree](https://www.sourcetreeapp.com/) that you can leverage.

##### Git global setup

git config --global user.name "Marty Zigman"
git config --global user.email "marty.zigman@prolect.com"


##### Checking out an existing repository

```
cd target_folder
git clone https://gitlab.com/prolecto-public/client-repo-template.git
```


##### Push an existing folder

```
cd existing_folder
git init
git remote add origin https://gitlab.com/prolecto-public/client-repo-template.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


##### Push an existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/prolecto-public/client-repo-template.git
git push -u origin --all
git push -u origin --tags
```

